# -*- coding: utf-8 -*-
from django.forms import ModelForm
from form_list.models import Top


class UserForm(ModelForm):
    '''ユーザーフォーム'''

    class Meta:
        model = Top
        fields = ('username', 'email', 'password')
