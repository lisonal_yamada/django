from django.db import models


# モデルとは、サイトを構成するデータの、唯一絶対的なデータソースを指します。 モデルには、保存したいデータを表すデータフィールドと、データのビヘイビアを 定義します。通常、一つのモデルは一つのデータベーステーブルに対応しています。
class Top(models.Model):
    '''トップページ'''
    username = models.CharField(u'ユーザー名', max_length=30)
    email = models.EmailField(u'メールアドレス', max_length=30)
    password = models.CharField(u'パスワード', max_length=30)

    class Meta:
        managed = False
        db_table = 'users'
