# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Top',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('username', models.CharField(verbose_name='ユーザー名', max_length=30)),
                ('email', models.CharField(verbose_name='メールアドレス', max_length=30)),
                ('password', models.CharField(verbose_name='パスワード', max_length=30)),
            ],
            options={
                'db_table': 'users',
                'managed': False,
            },
            bases=(models.Model,),
        ),
    ]
