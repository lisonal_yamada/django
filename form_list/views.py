# -*- coding: utf-8 -*-
import csv
from django.core.exceptions import MultipleObjectsReturned, ObjectDoesNotExist
from django.http.response import HttpResponse
from django.shortcuts import redirect, render
from django.views.generic.list import ListView
from form_list.forms import UserForm
from form_list.models import Top


class FormList(ListView):
    context_object_name = "user_list"  # querysetの名前を指定
    template_name = 'form_list.html'  # 使うテンプレート指定
    queryset = Top.objects.all().order_by('id')  # リストで表示したいデータを入れる（配列）
    paginate_by = 5  # リスト何個まで表示するか


class List(ListView):
    context_object_name = "user_list"  # querysetの名前を指定
    template_name = 'list.html'  # 使うテンプレート指定
    queryset = Top.objects.all().order_by('id')  # リストで表示したいデータを入れる（配列）
    paginate_by = 5  # リスト何個まで表示するか


def download(request):
    # 適切な CSV 用ヘッダとともに HttpResponse オブジェクトを生成します。
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="Userlist.csv"'
    writer = csv.writer(response)
    obj_all = Top.objects.all()
    for obj in obj_all:
        row = []
        for field in Top._meta.fields:
            row.append(getattr(obj, field.name))
        writer.writerow(row)
    return response


# TODO  クラスにする
def add_user(request):
    top = Top()
    if request.method == 'POST':
        form = UserForm(request.POST, instance=top)
        if form.is_valid():  # フォームバリデーションを行う
            try:
                user = Top.objects.get(username=request.POST.get('username'))
                # TODO 重複登録(できたらformで)
                if user is None:
                    top = form.save(commit=False)  # インスタンスを生成、ただし保存はしない
                    top.save()  # 新たなインスタンスを保存
                    return redirect('add_user')  # ページングのURL残しとくならここを変える
            except MultipleObjectsReturned or ObjectDoesNotExist:
                return render(request, 'add_user.html', dict(form=form))
    else:
        form = UserForm(instance=top)  # top インスタンスからフォームを作成
    return render(request, 'add_user.html', dict(form=form))
