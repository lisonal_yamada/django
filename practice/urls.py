# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.contrib import admin
from form_list import views
from form_list.views import FormList, List

urlpatterns = patterns('',
                       # Examples:
                       # url(r'^$', 'practice.views.home', name='home'),
                       # url(r'^blog/', include('blog.urls')),

                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^formlist/$', FormList.as_view(), name='form_list'),  # form用list
                       url(r'^list/$', List.as_view(), name='list'),  # listのみ
                       url(r'^add/$', views.add_user, name='add_user'),  # ユーザー登録
                       url(r'^download/$', views.download, name='download'),  # ダウンロード用
                       #url(r'^/$', views.top_page, name='top'),  # ダウンロード用
                       )
